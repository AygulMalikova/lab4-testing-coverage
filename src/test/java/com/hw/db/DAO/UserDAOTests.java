package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

public class UserDAOTests {
    private JdbcTemplate mockJdbc;
    private List<Object> dataToChange;
    private String nickname = "nickname";
    private String email = "email";
    private String fullname = "fullname";
    private String about = "about";


    @BeforeEach
    @DisplayName("User DAO tests set up")
    void beforeUserTests() {
        mockJdbc = mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mockJdbc);
        dataToChange=new ArrayList<>();
    }

    @Test
    @DisplayName("Change User #1: NONE")
    void testChange1() {
        User user = new User(nickname, null, null, null);
        UserDAO.Change(user);

        verifyNoInteractions(mockJdbc);
    }

    @Test
    @DisplayName("Change User #2: email")
    void testChange2() {
        User user = new User(nickname, email, null, null);
        dataToChange.add(email);
        dataToChange.add(nickname);

        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(dataToChange.get(0)),
                Mockito.eq(dataToChange.get(1)));
    }

    @Test
    @DisplayName("Change User #3: fullname")
    void testChange3() {
        User user = new User(nickname, null, fullname, null);
        dataToChange.add(fullname);
        dataToChange.add(nickname);

        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(dataToChange.get(0)),
                Mockito.eq(dataToChange.get(1)));
    }

    @Test
    @DisplayName("Change User #4: about")
    void testChange4() {
        User user = new User(nickname, null, null, about);
        dataToChange.add(about);
        dataToChange.add(nickname);

        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(dataToChange.get(0)),
                Mockito.eq(dataToChange.get(1)));
    }

    @Test
    @DisplayName("Change User #5: email & fullname")
    void testChange5() {
        User user = new User(nickname, email, fullname, null);
        dataToChange.add(email);
        dataToChange.add(fullname);
        dataToChange.add(nickname);

        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(dataToChange.get(0)),
                Mockito.eq(dataToChange.get(1)),
                Mockito.eq(dataToChange.get(2)));
    }

    @Test
    @DisplayName("Change User #6: email & about")
    void testChange6() {
        User user = new User(nickname, email, null, about);
        dataToChange.add(email);
        dataToChange.add(about);
        dataToChange.add(nickname);

        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(dataToChange.get(0)),
                Mockito.eq(dataToChange.get(1)),
                Mockito.eq(dataToChange.get(2)));
    }

    @Test
    @DisplayName("Change User #7: fullname & about")
    void testChange7() {
        User user = new User(nickname, null, fullname, about);
        dataToChange.add(fullname);
        dataToChange.add(about);
        dataToChange.add(nickname);

        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(dataToChange.get(0)),
                Mockito.eq(dataToChange.get(1)),
                Mockito.eq(dataToChange.get(2)));
    }

    @Test
    @DisplayName("Change User #8: email, fullname & about")
    void testChange8() {
        User user = new User(nickname, email, fullname, about);
        dataToChange.add(email);
        dataToChange.add(fullname);
        dataToChange.add(about);
        dataToChange.add(nickname);

        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(dataToChange.get(0)),
                Mockito.eq(dataToChange.get(1)),
                Mockito.eq(dataToChange.get(2)),
                Mockito.eq(dataToChange.get(3))
        );
    }
}
